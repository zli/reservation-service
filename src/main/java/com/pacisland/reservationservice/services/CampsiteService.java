package com.pacisland.reservationservice.services;


import com.pacisland.reservationservice.exceptions.ResourceNotFoundException;
import com.pacisland.reservationservice.models.Campsite;
import com.pacisland.reservationservice.models.User;
import com.pacisland.reservationservice.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class CampsiteService {

    @Autowired
    private UserRepository userRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public List<Campsite> findAll(Integer pageNumber, Integer pageSize) {
        if(pageNumber == null || pageNumber < 1){
            pageNumber = 1;
        }
        if(pageSize == null || pageSize < 30 ){
            pageSize = 30;
        }
        TypedQuery<Campsite> query = entityManager.createQuery("select c from Campsite c where c.reservableDate > :today", Campsite.class);
        query.setParameter("today", new Date());
        query.setFirstResult((pageNumber-1) * pageSize);
        query.setMaxResults(pageSize);
        return query.getResultList();
    }

    @Transactional
    public Campsite save(Campsite camp) {
        entityManager.persist(camp);
        return camp;
    }

    @Transactional
    public String reserveCampsites(List<Date> listDate, User user){
        // check date range constraints
        checkDateRange(listDate);
        User userEntity = userRepository.findByEmail(user.getEmail());
        if(userEntity==null){
            //create new user
            userEntity = userRepository.saveAndFlush(user);
        }
        Long userId = userEntity.getId();
        checkMaxDays(userId, listDate);
        checkAvailable(listDate);
        String bookingIdentifier = null;
        try{
            bookingIdentifier = updateReserveByUserId(userId, listDate);
        }catch(ConcurrencyFailureException ex){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Conflict with other reservations. Please reselect date(s)");
        }
        return bookingIdentifier;
    }


    private User checkUserId(Long userId) {
        return userRepository.findById(userId).orElseThrow(
                ()-> new ResourceNotFoundException("User not found: "+ userId)

        );
    }

    private void checkDateRange(List<Date> listDate) throws ResponseStatusException{
        // date range constraints
        if(listDate==null || listDate.isEmpty()){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "The reserve arrival date(s) is empty");
        }
        listDate.forEach(date -> {
            if (date.before(DateUtils.addDays(new Date(), 1))){
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "The campsite can be reserved minimum 1 day(s) ahead of arrival");
            }
            if (date.after(DateUtils.addMonths(new Date(), 1))){
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "The campsite can be reserved up to 1 month in advance.");
            }
        });
    }

    @Transactional
    void checkMaxDays(Long userId, List<Date> listDate) throws ResponseStatusException {
        TypedQuery<Campsite> query = entityManager.createQuery("select c from Campsite c where c.user.id = :user_id", Campsite.class);
        query.setParameter("user_id", userId);
        query.setLockMode(LockModeType.OPTIMISTIC);
        List<Campsite> campsites = query.getResultList();
        Set<Date> reserveDate = Stream
                .concat(
                        campsites.stream()
                                .map(Campsite::getReservableDate)
                                .collect(Collectors.toSet())
                                .stream(),
                        listDate
                                .stream())
                .collect(Collectors.toSet());
        if(reserveDate.size()>3){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "The campsite can be reserved for max 3 days");
        }
    }

    @Transactional
    public void checkAvailable(List<Date> listDate) throws ResponseStatusException {
        TypedQuery<Campsite> query = entityManager.createQuery("select c from Campsite c where c.reservableDate in :reservable_date", Campsite.class);
        query.setParameter("reservable_date", listDate);
        query.setLockMode(LockModeType.OPTIMISTIC_FORCE_INCREMENT);
        List<Campsite> campsites = query.getResultList();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        for (Campsite campsite : campsites) {
            if (campsite.getUser() != null) {
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "The date " + formatter.format(campsite.getReservableDate()) + " is already reserved");
            }
        }
    }

    @Transactional
    String updateReserveByUserId(Long userId, List<Date> listDate) throws RuntimeException {
        checkUserId(userId);
        String bookingIdentifier = RandomStringUtils.random(10, true, true);
        Query query = entityManager.createQuery("update Campsite c set c.user.id = :user_id, c.bookingIdentifier = :booking_identifier where c.reservableDate in :reservable_date");
        query.setParameter("user_id", userId);
        query.setParameter("booking_identifier", bookingIdentifier);
        query.setParameter("reservable_date", listDate);
        query.executeUpdate();
        return bookingIdentifier;
    }

    @Transactional
    public List<Campsite> reviewReservation(String bookingIdentifier) {
        TypedQuery<Campsite> query = entityManager.createQuery("select c from Campsite c where c.bookingIdentifier = :booking_identifier", Campsite.class);
        query.setParameter("booking_identifier", bookingIdentifier);
        query.setLockMode(LockModeType.OPTIMISTIC);
        return query.getResultList();
    }

    @Transactional
    public void cancelReservation(String bookingIdentifier) {
        Query query = entityManager.createQuery("update Campsite c set c.bookingIdentifier = null, c.user = null where c.bookingIdentifier = :booking_identifier");
        query.setParameter("booking_identifier", bookingIdentifier);
        query.executeUpdate();
    }

    @Transactional
    Long getUserIdByBookingIdentifier(String bookingIdentifier) {
        Query query = entityManager.createQuery("select c from Campsite c where c.bookingIdentifier = :booking_identifier", Campsite.class);
        query.setParameter("booking_identifier", bookingIdentifier);
        query.setMaxResults(1);
        Campsite campsite = (Campsite)query.getSingleResult();
        if(campsite != null && campsite.getUser()!=null){
            return campsite.getUser().getId();
        }else{
            return null;
        }
    }

    @Transactional
    public String modifyReservation(String bookingIdentifier, List<Date> listDate){
        Long userId = getUserIdByBookingIdentifier(bookingIdentifier);
        if(userId==null){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "The booking identifier " + bookingIdentifier + " is not existed.");
        }
        // cancel existed reservation
        cancelReservation(bookingIdentifier);
        // check availability
        checkAvailable(listDate);
        // update new reservation
        return updateReserveByUserId(userId, listDate);
    }



}
