package com.pacisland.reservationservice.controllers;


import com.pacisland.reservationservice.exceptions.ResourceNotFoundException;
import com.pacisland.reservationservice.models.User;
import com.pacisland.reservationservice.repositories.UserRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Api
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/users")
    public User addUser(@RequestBody User user){
        User userEntity = null;
        try{
            userEntity = userRepository.save(user);
        }
        catch(DataIntegrityViolationException ex){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "constraint violation", ex);
        }
        return userEntity;
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> findAll(){
        List<User> result = new ArrayList<>();
        userRepository.findAll().forEach(result::add);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<User> findUserId(@PathVariable(value = "id")
                                                   Long id){

        User user = userRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException("User not found: "+ id)

        );
        return ResponseEntity.ok().body(user);
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable(value = "id") Long id){
        User user = userRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException("User not found: "+ id)

        );
        userRepository.delete(user);
        return ResponseEntity.ok().build();
    }
}
