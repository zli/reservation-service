package com.pacisland.reservationservice.controllers;

import com.pacisland.reservationservice.models.Campsite;
import com.pacisland.reservationservice.models.User;
import com.pacisland.reservationservice.repositories.CampsiteRepository;
import com.pacisland.reservationservice.services.CampsiteService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Api
@RestController
public class CampsiteController {

    @Autowired
    private CampsiteService campsiteService;

    @PostMapping("/campsites")
    public Campsite addCampsite(@RequestBody Campsite camp){
        return campsiteService.save(camp);
    }

    @GetMapping("/campsites")
    @Cacheable("campsites")
    public ResponseEntity<List<Campsite>> findAll(@RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) Integer pageSize){

        return ResponseEntity.ok(campsiteService.findAll(pageNumber, pageSize));
    }

    @PostMapping("/reservations")
    public ResponseEntity<String> reserve(@RequestParam("dates") @DateTimeFormat(pattern = "yyyy-MM-dd") List<Date> listDate, @ModelAttribute("user") User user){
        String bookingIdentifier = campsiteService.reserveCampsites(listDate, user);
        return ResponseEntity.ok().body(bookingIdentifier);
    }

    @GetMapping("/reservation/{bookingIdentifier}")
    public ResponseEntity<List<Campsite>> reviewReservation(@PathVariable String bookingIdentifier){
        List<Campsite> campsites = campsiteService.reviewReservation(bookingIdentifier);
        return ResponseEntity.ok().body(campsites);
    }

    @PutMapping("/reservation/{bookingIdentifier}")
    public ResponseEntity<String> modifiyReservation(@PathVariable String bookingIdentifier, @RequestParam("dates") @DateTimeFormat(pattern = "yyyy-MM-dd") List<Date> listDate){
        String newBookingIdentifier = campsiteService.modifyReservation(bookingIdentifier, listDate);
        return ResponseEntity.ok().body(newBookingIdentifier);
    }

    @DeleteMapping("/reservation/{bookingIdentifier}")
    public ResponseEntity<String> cancelReservation(@PathVariable String bookingIdentifier){
        campsiteService.cancelReservation(bookingIdentifier);
        return ResponseEntity.ok().build();
    }

}
