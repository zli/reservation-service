package com.pacisland.reservationservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "campsites")
public class Campsite implements Serializable {

    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "campsite_id_seq", sequenceName = "campsite_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "campsite_id_seq")
    private Long id;

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "reservable_date")
    private Date reservableDate;

    @Column(name = "booking_identifier")
    @JsonIgnore
    private String bookingIdentifier;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable=true)
    private User user;

    @Version
    @JsonIgnore
    private Long version;

    @Transient
    @JsonSerialize
    private Boolean isAvailable() {
        return user==null;
    };
}
