package com.pacisland.reservationservice.repositories;

import com.pacisland.reservationservice.models.Campsite;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CampsiteRepository extends CrudRepository<Campsite, Integer> {
}
