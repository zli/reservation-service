insert
into campsites(reservable_date)
select day::date
FROM generate_series(current_date, current_date + interval '365' day, INTERVAL '1 day') day;