create sequence if not exists campsite_id_seq;
create sequence if not exists user_id_seq;
create table if not exists campsites (id integer DEFAULT nextval('campsite_id_seq'::regclass) not null, reservable_date date, booking_identifier varchar(255), version integer default 0, user_id integer, primary key (id));
create table if not exists users (id integer DEFAULT nextval('user_id_seq'::regclass) not null, email varchar(255), first_name varchar(255), last_name varchar(255), primary key (id));