package com.pacisland.reservationservice.services;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import com.pacisland.reservationservice.models.Campsite;
import com.pacisland.reservationservice.models.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.server.ResponseStatusException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@Slf4j
@ActiveProfiles("test")
public class CampsiteServiceTest {

    @Autowired
    CampsiteService campsiteService;

    final FakeValuesService fakeValuesService = new FakeValuesService(
            new Locale("en-us"), new RandomService());
    final Faker faker = new Faker();

    User user = null;

    final List<Date> listDateValid = new LinkedList<>();
    final List<Date> listDateSameDay = new LinkedList<>();
    final List<Date> listDateAfterMonth = new LinkedList<>();
    final List<Date> listDate4Days = new LinkedList<>();
    final List<Date> listDate2Days = new LinkedList<>();

    @BeforeEach
    void setup() {
        Date date = new Date();
        for (int i = 0; i < 365; i++) {
            Campsite campsite = new Campsite();
            campsite.setReservableDate(DateUtils.addDays(date, i));
            campsiteService.save(campsite);
        }

        user = new User();
        user.setEmail(fakeValuesService.bothify("????##@gmail.com"));
        user.setLastName(faker.name().lastName());
        user.setFirstName(faker.name().firstName());

        listDateValid.add(DateUtils.addDays(date, 2));
        listDateValid.add(DateUtils.addDays(date, 3));
        listDateValid.add(DateUtils.addDays(date, 4));

        listDate2Days.add(DateUtils.addDays(date, 29));
        listDate2Days.add(DateUtils.addDays(date, 30));

        listDate4Days.add(DateUtils.addDays(date, 2));
        listDate4Days.add(DateUtils.addDays(date, 3));
        listDate4Days.add(DateUtils.addDays(date, 4));
        listDate4Days.add(DateUtils.addDays(date, 5));

        listDateSameDay.add(date);
        listDateAfterMonth.add(DateUtils.addDays(date, 32));
    }

    @Test
    void findAllwithParameters() {
        List<Campsite> campsites = campsiteService.findAll(1, 30);
        assertEquals(campsites.size(), 30);
    }

    @Test
    void findAllwithoutParameters() {
        List<Campsite> campsites = campsiteService.findAll(null, null);
        assertEquals(campsites.size(), 30);
    }

    @Test
    @Rollback
    void save() {
        Campsite campsite = new Campsite();
        Date date = new Date();
        campsite.setReservableDate(DateUtils.addDays(date, 366));
        campsiteService.save(campsite);
        assertNotNull(campsite.getId());
    }

    @Test
    @Rollback
    void userReserveWithValidValues() {
        assertNotNull(campsiteService.reserveCampsites(listDateValid, user));
    }

    @Test
    @Rollback
    void userReserveMoreThan3Days_returnError() {
        Assertions.assertThrows(ResponseStatusException.class, () -> campsiteService.reserveCampsites(listDate4Days, user));
    }


    @Test
    @Rollback
    void userReserveTheSameDay_returnError() {

        Assertions.assertThrows(ResponseStatusException.class, () -> campsiteService.reserveCampsites(listDateSameDay, user));
    }

    @Test
    @Rollback
    void userReserveNextMonth_returnError() {
        Assertions.assertThrows(ResponseStatusException.class, () -> campsiteService.reserveCampsites(listDateAfterMonth, user));
    }

    @Test
    void userReserveAndReview() {
        String bookingIdentifier = campsiteService.reserveCampsites(listDateValid, user);
        List<Campsite> campsites = campsiteService.reviewReservation(bookingIdentifier);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        List<String> listDate = listDateValid.stream().map(dateFormat::format).collect(Collectors.toList());
        campsites.stream().map(Campsite::getReservableDate).forEach(date -> assertTrue(listDate.contains(dateFormat.format(date))));
        campsiteService.cancelReservation(bookingIdentifier);
    }

    @Test
    @Rollback
    void userReserveAndCancel() {

        String bookingIdentifier = campsiteService.reserveCampsites(listDateValid, user);
        campsiteService.cancelReservation(bookingIdentifier);
        List<Campsite> campsites = campsiteService.reviewReservation(bookingIdentifier);
        assertEquals(campsites.size(), 0);
    }

    @Test
    void userReserveAndModify() {
        String bookingIdentifier = campsiteService.reserveCampsites(listDateValid, user);

        String newBookingIdentifier = campsiteService.modifyReservation(bookingIdentifier, listDate2Days);
        List<Campsite> campsites = campsiteService.reviewReservation(newBookingIdentifier);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        List<String> listDate = listDate2Days.stream().map(dateFormat::format).collect(Collectors.toList());
        campsites.stream().map(Campsite::getReservableDate).forEach(date -> assertTrue(listDate.contains(dateFormat.format(date))));
        campsiteService.cancelReservation(newBookingIdentifier);
    }

    @Test
    void multipleUsersReserveSameDay() {
        int clientTotal = 10;
        int threadTotal = 5;
        AtomicLong winnerId = new AtomicLong(0);
        AtomicReference<String> bookingId = new AtomicReference<>();
        try {
            ExecutorService executorService = Executors.newCachedThreadPool();
            final Semaphore semaphore = new Semaphore(threadTotal);
            final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
            for (int i = 0; i < clientTotal ; i++) {
                executorService.execute(() -> {
                    try {
                        semaphore.acquire();
                        Faker faker = new Faker();
                        User user = new User();
                        user.setEmail(faker.bothify("????##@gmail.com"));
                        user.setLastName(faker.name().lastName());
                        user.setFirstName(faker.name().firstName());
                        String bookingIdentifier = campsiteService.reserveCampsites(listDateValid, user);
                        if(bookingIdentifier!=null){
                            winnerId.set(user.getId());
                            bookingId.set(bookingIdentifier);
                            log.info("reserve succeed: " + bookingIdentifier);
                        }
                        semaphore.release();
                    } catch (ConcurrencyFailureException ex) {
                        log.error("ConcurrencyFailureException: " + ex.getMessage());
                    }catch (ResponseStatusException ex){
                        log.error("ResponseStatusException: " + ex.getMessage());
                    }catch(Exception e){
                        log.error("exception", e);
                    }finally {
                        semaphore.release();
                    }
                    countDownLatch.countDown();
                });
            }
            countDownLatch.await();
            executorService.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assertions.assertEquals(campsiteService.getUserIdByBookingIdentifier(bookingId.get()), winnerId.get());
        campsiteService.cancelReservation(bookingId.get());
    }

}
