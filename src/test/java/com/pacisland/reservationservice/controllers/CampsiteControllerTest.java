package com.pacisland.reservationservice.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pacisland.reservationservice.models.Campsite;
import com.pacisland.reservationservice.models.User;
import com.pacisland.reservationservice.services.CampsiteService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class CampsiteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CampsiteService campsiteService;

    User user = null;
    Date day1 = null;
    Date day2 = null;
    Date day3 = null;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    List<Date> listDate = new LinkedList<>();

    @BeforeEach
    void setUp() throws Exception {
        user = User.builder().id(1L).email("alex@gamil.com").build();

        day1 = formatter.parse(formatter.format(DateUtils.addDays(new Date(), 2)));
        day2 = formatter.parse(formatter.format(DateUtils.addDays(new Date(), 3)));
        day3 = formatter.parse(formatter.format(DateUtils.addDays(new Date(), 4)));
        listDate.add(day1);
        listDate.add(day2);
        listDate.add(day3);
    }

    @Test
    void addCampsite() {
    }

    @Test
    void findAll() throws Exception{

        when(campsiteService.findAll(null, null)).thenReturn(Lists.newArrayList(
                Campsite.builder().id(1L).user(user).reservableDate(day1).build(),
                Campsite.builder().id(2L).user(user).reservableDate(day2).build(),
                Campsite.builder().id(3L).reservableDate(day3).build()
        ));

        this.mockMvc.perform(get("/campsites")).andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(3)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].reservableDate", is(formatter.format(day1)+"T04:00:00.000+00:00")))
                .andExpect(jsonPath("$[0].available", is(false)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].reservableDate", is(formatter.format(day2)+"T04:00:00.000+00:00")))
                .andExpect(jsonPath("$[1].available", is(false)))
                .andExpect(jsonPath("$[2].id", is(3)))
                .andExpect(jsonPath("$[2].reservableDate", is(formatter.format(day3)+"T04:00:00.000+00:00")))
                .andExpect(jsonPath("$[2].available", is(true)))
        ;

    }

    @Test
    void reserve() throws Exception {
        String bookingIdentifier = RandomStringUtils.random(10, true, true);
        when(campsiteService.reserveCampsites(listDate, user)).thenReturn(bookingIdentifier);

        this.mockMvc.perform(post("/reservations")
                        .param("id", String.valueOf(user.getId()))
                        .param("email", user.getEmail())
                        .param("firstName", user.getFirstName())
                        .param("lastName", user.getLastName())
                        .param("dates", formatter.format(day1), formatter.format(day2), formatter.format(day3))
        )
                .andExpect(status().isOk())
                .andExpect(content().string(bookingIdentifier));
    }

    @Test
    void reviewReservation() throws Exception {
        String bookingIdentifier = RandomStringUtils.random(10, true, true);
        when(campsiteService.reviewReservation(bookingIdentifier)).thenReturn(Lists.newArrayList(
                Campsite.builder().id(1L).user(user).reservableDate(day1).build(),
                Campsite.builder().id(2L).user(user).reservableDate(day2).build()
        ));

        this.mockMvc.perform(get("/reservation/"+bookingIdentifier))
                .andExpect(jsonPath("$[*]", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].reservableDate", is(formatter.format(day1)+"T04:00:00.000+00:00")))
                .andExpect(jsonPath("$[0].available", is(false)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].reservableDate", is(formatter.format(day2)+"T04:00:00.000+00:00")))
                .andExpect(jsonPath("$[1].available", is(false)));
    }

    @Test
    void modifiyReservation() throws Exception{
        String bookingIdentifier = RandomStringUtils.random(10, true, true);
        String newBookingIdentifier = RandomStringUtils.random(10, true, true);
        when(campsiteService.modifyReservation(bookingIdentifier, listDate)).thenReturn(newBookingIdentifier);

        this.mockMvc.perform(put("/reservation/"+bookingIdentifier)
                .param("dates", formatter.format(day1), formatter.format(day2), formatter.format(day3))
        )
                .andExpect(status().isOk())
                .andExpect(content().string(newBookingIdentifier));

    }

    @Test
    void cancelReservation() throws Exception {
        String bookingIdentifier = RandomStringUtils.random(10, true, true);

        this.mockMvc.perform(put("/reservation/"+bookingIdentifier)
                .param("dates", formatter.format(day1), formatter.format(day2), formatter.format(day3))
        )
                .andExpect(status().isOk());
    }
}