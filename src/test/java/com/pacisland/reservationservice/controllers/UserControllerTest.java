package com.pacisland.reservationservice.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pacisland.reservationservice.models.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void whenValidUserInfo_thenReturn200() throws Exception{
        User user = User.builder().firstName("Alex").lastName("Joe").email("alex@hotmail.com").build();

        mockMvc.perform(post("/users")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk());
    }

    @Test
    void whenRepeatedUserEmail_thenReturn400() throws Exception{
        User user1 = User.builder().firstName("Alex").lastName("Joe").email("alex1@hotmail.com").build();
        User user2 = User.builder().firstName("Def").lastName("Orj").email("alex1@hotmail.com").build();
        mockMvc.perform(post("/users")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(user1)))
                .andExpect(status().isOk());
        mockMvc.perform(post("/users")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(user2)))
                .andExpect(status().is4xxClientError());
    }
}
