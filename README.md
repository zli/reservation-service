# Read Me First
### Description

Back-end Tech Challenge

### Features
- optimistic locking is used to handle concurrency. It is ideal if the conflict is less than 20%.
- simulation for multiple users concurrency testing in multipleUsersReserveSameDay() in CampsiteServiceTest
- solution for large volume of requests use CacheManager. Redis is ideal but not selected for making sure the project could be built without issue.
- h2 is configured for unit test
- swagger2 is integrated, new version in path http://localhost:8080/swagger-ui/

### Build and test
```
 gradlew build
```

### Run in container
```
 gradlew bootJar

 docker-compose build

 docker-compose up
```

### Known issues
- First time running in container, PostgreSql service will be ready after web service, that causes the web server error.
